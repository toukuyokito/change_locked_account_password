""" Test for module change_pass
"""

import os
import pytest
from io import StringIO
import change_pass


class FakeChannel:
    """Imitation of paramiko.Channel"""

    def __init__(self, payload: str):
        """Constructor of FakeChannel

        :param payload: message to be encode
        :type payload: str
        """
        self.payload = bytes(payload.encode())

    def recv_ready(self) -> bool:
        """Allways return True

        :return: True
        :rtype: bool
        """
        return True

    # pylint: disable=W0613
    # recv imitates paramiko.Channel.rcv() signature
    # but do not use nbytes
    def recv(self, nbytes: int) -> bytes:
        """Immitation of paramiko.Channel.rcv()

        :param nbytes: Signature imitation not used
        :type nbytes: int
        :return: Encoded payload to bytes
        :rtype: bytes
        """
        return self.payload


def test_one_of_patterns_are_matche():
    """Tests when function one_of_patterns_are_matche must return True"""
    assert change_pass.one_of_patterns_are_matche(["Orange", "bananna"], "Orange")
    assert change_pass.one_of_patterns_are_matche(["Orange", "bananna"], "orange")
    assert change_pass.one_of_patterns_are_matche(["orange", "bananna"], "Orange")


def test_one_of_patterns_are_not_matche():
    """Test when function one_of_patterns_are_matche must return False"""
    assert not change_pass.one_of_patterns_are_matche(["Orange", "bananna"], "poire")


def test_input_list_mode_from_cli_interactive_mode_get_machines():
    """Test the parsing output when the inventory came from cli"""
    result = change_pass.interactive_mode_get_machines(
        change_pass.InputType.FROM_CLI, machines="machine1,machine2"
    )
    assert result == ["machine1", "machine2"]


@pytest.fixture
def fixure_create_machine_inventory(tmp_path: "pytest.fixture"):
    """Simulate an machines inventory create by an user

    :param tmp_path: Pytest fixure
    :type tmp_path: pytest.fixture
    :return: Path of ephemere inventory
    :rtype: String
    """
    target_output = os.path.join(tmp_path, "machines")

    with open(target_output, "w", encoding="utf-8") as file:
        file.write("       machine1, machine2,machine3   \n")

    return target_output


# pylint: disable=W0621
# Seem to be incompatible with pytest fixure
def test_input_list_mode_from_file_interactive_mode_get_machines(
    fixure_create_machine_inventory,
):
    """Test the parsing output when the inventory came from file"""
    result = change_pass.interactive_mode_get_machines(
        change_pass.InputType.FROM_FILE, inventory_path=fixure_create_machine_inventory
    )
    assert result == ["machine1", "machine2", "machine3"]


def test_input_list_mode_unknown_interactive_mode_get_machines():
    """Test the parsing output when passing a unknown mode"""
    with pytest.raises(ValueError):
        change_pass.interactive_mode_get_machines(
            None, machines="machine1, machine2,machine3"
        )


# change_expired_password_over_ssh
@pytest.mark.asyncio
async def test_async_change_expired_password_over_ssh():
    """Test when password failed to change if the return value is an error tuple"""
    host_ip = "127.0.0.1"
    result = await change_pass.async_change_expired_password_over_ssh(
        host=host_ip,
        username="user",
        current_password="123",
        new_password="124",
        patterns="a,b",
    )

    assert isinstance(result, tuple)
    assert result[0] == host_ip
    assert result[1] == "failed"
    assert isinstance(result[2], str)


def test_wait_until_one_of_patterns_matche():
    """Tests for function wait_until_one_of_patterns_matche :
    Test a case when the function must found à pattern
    Test a case when the function must not found a pattern then raise a timeout error
    """
    assert not change_pass.wait_until_one_of_patterns_matche(
        FakeChannel("test"), "test,orange", 0.10
    )
    with pytest.raises(TimeoutError):
        change_pass.wait_until_one_of_patterns_matche(
            FakeChannel("test"), "tests,orange", 0.10
        )


def test_interactive_mode_gatherer_information(monkeypatch):
    """Test for function interactive_mode_gatherer_information
    Test when a user write a valide input

    :param monkeypatch: pytest.fixture
    :type monkeypatch: pytest.fixture
    """
    username = "user"
    old_password = "123"
    new_password = "124"

    monkeypatch.setattr(
        "sys.stdin",
        StringIO(f"{username}\n"),
    )

    responses_to_getpass = iter(
        [f"{old_password}", f"{new_password}", f"{new_password}"]
    )
    monkeypatch.setattr("getpass.getpass", lambda _: next(responses_to_getpass))

    result = change_pass.interactive_mode_gatherer_information()

    assert result[0] == username
    assert result[1] == old_password
    assert result[2] == new_password


def test_interactive_mode_gatherer_information_exit(monkeypatch):
    """Test for function interactive_mode_gatherer_information
    Test when a user failed to confirm password and reach the max attemp

    :param monkeypatch: pytest.fixture
    :type monkeypatch: pytest.fixture
    """
    username = "user"

    monkeypatch.setattr(
        "sys.stdin",
        StringIO(f"{username}\n"),
    )

    responses_to_getpass = iter([str(i) for i in range(13)])
    monkeypatch.setattr("getpass.getpass", lambda _: next(responses_to_getpass))

    with pytest.raises(SystemExit):
        change_pass.interactive_mode_gatherer_information()
