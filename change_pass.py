"""Allow to change password when an account is lock
"""
import paramiko
import getpass
import time
import log
from contextlib import closing
from enum import Enum
import asyncio
import logging
import sys

MACHINE_SEPARATOR = ","
CONFIG_SPLIT_SEPARATOR = ","


class InputType(Enum):
    """Enum"""

    FROM_CLI: int = 1
    FROM_FILE: int = 2


def interactive_mode_gatherer_information() -> tuple:
    """Display a promt to fetch target : username, old and new password from cli
    Close the programme if a certain amount of wrong input is reached

    :return: A tuple container the username, the old password and the new password
    :rtype: tuple(str, str, str)
    """
    ssh_username = input("username > ")
    print("Don't worry password input is not display in prompt")
    old_ssh_pass = getpass.getpass(f"Current ssh password for user {ssh_username} > ")

    new_pass = ""
    confirmation = ""
    max_attempt = 5
    current_attempt = 1

    while True:
        new_pass = getpass.getpass(f"New ssh password for user {ssh_username} > ")
        confirmation = getpass.getpass("Confirm password > ")
        current_attempt += 1

        if new_pass == confirmation:
            break

        log.print_yellow(f"Attempt {current_attempt} on {max_attempt}")
        logging.warning(f"Attempt {current_attempt} on {max_attempt}")

        if current_attempt == max_attempt:
            log.print_orange("Max attempt reached end of programe")
            logging.fatal("Max attempt reached end of programe")
            sys.exit()
        else:
            log.print_red("Confirmation and password doesn't match please retype")
            logging.error("Confirmation and password doesn't match please retype")

    return ssh_username, old_ssh_pass, new_pass


def interactive_mode_get_machines(
    mode: InputType, inventory_path: str = None, machines: str = None
) -> list[str]:
    """Parse the input from cli

    :param mode: Read from cli or file
    :type input_list_mode: InputType
    :param file_path: Only use if InputType.FROM_FILE
    :param machines: Only use if InputType.FROM_CLI must be a str of machine separate with : ,
    :type machines: str
    :return: A list of machines
    :rtype: list[str]
    """

    output_value = ""
    if mode == InputType.FROM_CLI:
        output_value = machines

    if mode == InputType.FROM_FILE:
        with open(inventory_path, "r", encoding="utf-8") as file:
            output_value = file.read()

    if output_value == "":
        raise ValueError(f"Operation {mode} are unknown")
    output_value = output_value.replace(" ", "")
    output_value = output_value.replace("\n", "")

    return output_value.split(MACHINE_SEPARATOR)


def one_of_patterns_are_matche(patterns: list[str], string_from_buffer: str) -> bool:
    """Test if string_from_buffer container one of str store in patterns

    :param patterns: Stored list of str to search
    :type patterns: list[str]
    :param string_from_buffer: Str which can contain any of the patterns.
    :type string_from_buffer: str
    :return: True if any patterns are found else Fasle
    :rtype: bool
    """
    logging.debug(f"one_of_patterns_are_matche : {string_from_buffer=}")

    for pattern in patterns:

        if string_from_buffer.lower().find(pattern.lower()) != -1:
            return True

    return False


def wait_until_one_of_patterns_matche(
    channel: "paramiko.Channel", patterns: str, timeout_in_seconds: int = 15
):
    """Wait from target to send a promp with any of patterns

    :param channel: paramiko.Channel
    :type channel: paramiko.Channel
    :param patterns: A list of pattern separate by : ,
    :type patterns: str
    :param timeout_in_seconds: Timeout in seconds, defaults to 15
    :type timeout_in_seconds: int, optional
    :raises TimeoutError: Raised when timeout_in_seconds is exceed
    """

    patterns = patterns.lower().split(CONFIG_SPLIT_SEPARATOR)
    logging.debug(f"{patterns=}")

    trigger_timeout = time.time() + timeout_in_seconds

    while True:
        channel_read_buffer = ""
        if channel.recv_ready():
            channel_read_buffer = channel.recv(4096).decode("utf-8").lower()
        if channel_read_buffer != "" and one_of_patterns_are_matche(
            patterns, channel_read_buffer
        ):
            break
        if time.time() > trigger_timeout:
            raise TimeoutError(
                f"Timeout while waiting for '{patterns}', channel content : '{channel_read_buffer}'"
            )


def change_expired_password_over_ssh(
    host: str, username: str, current_password: str, new_password: str, patterns: tuple
):
    """Change expired account password with paramiko

    :param host: hostname or IP
    :type host: str
    :param username: username use to connect with ssh
    :type username: str
    :param current_password: password use to connect
    :type current_password: str
    :param new_password: password to set
    :type new_password: str
    :param patterns: A tuple of str contening pattern whitch are separaded by comma
    :type patterns: tuple
    """
    log.print_green(f"Start changing password for {host}")
    logging.info(f"Start changing password for {host}")

    with closing(paramiko.SSHClient()) as ssh_connection:
        ssh_connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_connection.connect(
            hostname=host, username=username, password=current_password
        )
        ssh_channel = ssh_connection.invoke_shell()

        wait_until_one_of_patterns_matche(ssh_channel, patterns[0])
        ssh_channel.send(f"{current_password}\n")

        wait_until_one_of_patterns_matche(ssh_channel, patterns[1])
        ssh_channel.send(f"{new_password}\n")

        wait_until_one_of_patterns_matche(ssh_channel, patterns[2])
        ssh_channel.send(f"{new_password}\n")

        wait_until_one_of_patterns_matche(ssh_channel, patterns[3])


async def async_change_expired_password_over_ssh(
    host: str, username: str, current_password: str, new_password: str, patterns: tuple
) -> tuple:
    """Async wrapper for change_expired_password_over_ssh, change expired account password with paramiko

    :param host: hostname or IP
    :type host: str
    :param username: username use to connect with ssh
    :type username: str
    :param current_password: password use to connect
    :type current_password: str
    :param new_password: password to set
    :type new_password: str
    :param patterns: A tuple of str contening pattern whitch are separaded by comma
    :type patterns: tuple
    """
    try:
        await asyncio.to_thread(
            change_expired_password_over_ssh,
            host=host,
            username=username,
            current_password=current_password,
            new_password=new_password,
            patterns=patterns,
        )
    # pylint: disable=W0703
    # Because we want to catch all error for logging and print
    except Exception as generic_exception:
        return (host, "failed", str(generic_exception))

    return (host, "sucess", "")
