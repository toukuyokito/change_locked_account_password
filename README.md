# change_password_expired_account

## Requirements

This script was tested for python3.10

## Description
This script allows to change a password of an expired account.

You can specify one or more machines. Write your inventory in a file or pass as an argument

## Installation

````bash
# Optional use a virtual environment
python3.10 -m venv .venv
source .venv/bin/activate

# Then install required modules
pip install -r requirement.txt
````

## Usage

````bash
python change_locked_account_password.py -h

usage: change_locked_account_password.py [-h] [--inventory_help] [--get_exemples] {prompt,whitout_prompt} ...

Tool for changing locked account password

positional arguments:
  {prompt,whitout_prompt}
                        sub-command help
    prompt              Get necessary information from a prompt
    whitout_prompt      Define directly necessary information as an argument

options:
  -h, --help            Show this help message and exit
  --inventory_help      Get help for inventory notion
  --get_exemples        Get some execution examples
````

## Example of usage
````
python change_locked_account_password.py --get_exemples
Exemples :

  python change_locked_account_password.py prompt myinventory.txt
  python change_locked_account_password.py prompt --input_list machine1.exemple,machine2.exemple,10.0.0.1
  python change_locked_account_password.py whitout_prompt username current_password new_password machine1.exemple,machine2.exemple,10.0.0.1

````

## Configuration

You can custom the script with the file config.ini.

The default config file is : 
````ini

[log_level]
; Possible value debug, info
level = info

[patterns]
; String must be separate by comma exemple pattern1,pattern2,etc ...
; Case insensibility 
attempt_pattern_prompt1 = actuel,current

; String must be separate by comma exemple pattern1,pattern2,etc ...
; Prompt Tape new password
attempt_pattern_prompt2 = nouveau,new

; String must be separate by comma exemple pattern1,pattern2,etc ...
; Prompt Retape new password

attempt_pattern_prompt3 = retapez,retype

; String must be separate by comma exemple pattern1,pattern2,etc ...
;Prompt when password sucefully change

attempt_pattern_prompt4 = successfull

````

## Q/A

### What is attempt_pattern_promptX?
When a connection is engage with an ssh client to a locked account the password can be asked to be changed.

````bash
# Exemple of connection to locked account 
ssh demo@machine_demo
demo@machine_demo's password: 
You are required to change your password immediately (administrator enforced).
You are required to change your password immediately (administrator enforced).

WARNING: Your password has expired.
You must change your password now and login again!
Changing password for demo.
Current password:
````

So 

- attempt_pattern_prompt1 corresponds to a message who asks to provide the current password
- attempt_pattern_prompt2 corresponds to a message who asks to provide a new password
- attempt_pattern_prompt3 corresponds to a message who asks to confirm the new password
- attempt_pattern_prompt4 corresponds to a message when the password has been successfully updated

### Why attempt_pattern_promptX contain two words separated by a comma?

Language and OS are multiples, so you can specify different language or message prompted by the target system with different pattern split by a comma.

## TODO

- Add CSV support for machines inventory to perform more complex operations