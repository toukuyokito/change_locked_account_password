"""Log module to configure logger and colored print
"""
import logging


def configure_logger(debug: bool, ouput_log_file_path: str):
    """Configure logger

    :param debug: If true display debug if False set debug level to info
    :type debug: bool
    :param ouput_log_file_path: File to write logs
    :type ouput_log_file_path: str
    """
    logging_level = logging.INFO
    if debug:
        logging_level = logging.DEBUG

    logging.basicConfig(
        level=logging_level,
        format="%(asctime)s %(levelname)s %(message)s",
        filename=ouput_log_file_path,
        filemode="a",
    )


def __print_generic(message: str, color: str):
    """Print a message with color then reset color

    :param message: message to display
    :type message: str
    :param color: color to display in format : \x1b[XX;X;XXXm" e.g : red = \x1b[38;5;196m
    :type color: str
    """
    reset = "\x1b[0m"
    print(f"{color}{message}{reset}")


def print_red(message: str):
    """Print a message to sdout in red

    :param message: Message to print
    :type message: str
    """
    red = "\x1b[38;5;196m"
    __print_generic(message, red)


def print_green(message: str):
    """Print a message to sdout in green

    :param message: Message to print
    :type message: str
    """
    green = "\x1b[32m"
    __print_generic(message, green)


def print_yellow(message):
    """Print a message to sdout in yellow

    :param message: Message to print
    :type message: str
    """
    yellow = "\x1b[38;5;3m"
    __print_generic(message, yellow)


def print_orange(message):
    """Print a message to sdout in orange

    :param message: Message to print
    :type message: str
    """
    orange = "\x1b[38;5;208m"
    __print_generic(message, orange)
