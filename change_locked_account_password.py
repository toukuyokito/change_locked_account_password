"""Main module for change_lock_account_password
"""
import change_pass
import log
import logging
import argparse
import configparser
import asyncio
import datetime
import sys
import os

USER_EXEC_PATH = os.getcwd()
os.chdir(os.path.dirname(os.path.realpath(__file__)))

CURRENT_DATE = datetime.datetime.now()

LOG_OUTPUT_NAME = os.path.join(
    USER_EXEC_PATH,
    f"{CURRENT_DATE.year}_{str(CURRENT_DATE.month).zfill(2)}_{str(CURRENT_DATE.day).zfill(2)}_execution.log",
)


def get_exemples():
    """Show some exemple for using this command"""
    basename = os.path.basename(__file__)
    print("Exemples :".title(), end="\n\n")
    print(f"  python {basename} prompt myinventory.txt")
    print(
        f"  python {basename} prompt --input_list machine1.exemple,machine2.exemple,10.0.0.1"
    )
    print(
        f"  python {basename} whitout_prompt username current_password new_password machine1.exemple,machine2.exemple,10.0.0.1",
        end="\n\n",
    )


def get_help_inventory_notion():
    """Diplay help in stdout for inventory notion"""
    print("Help for inventory notion :".title(), end="\n\n")
    print(
        "  An inventory is a inline file containing a list of IP or hostname separe by comma.",
        end="\n\n",
    )


def create_parser() -> "argparse.ArgumentParser.parse_args":
    """_summary_

    :return: a parser from argparse library
    :rtype: argparse.ArgumentParser.parse_args
    """
    parser = argparse.ArgumentParser(
        description="Tool for changing locked account password"
    )

    parser.add_argument(
        "--inventory_help", action="store_true", help="Get help for inventory notion"
    )

    parser.add_argument(
        "--get_exemples", action="store_true", help="Get some execution examples"
    )

    subparsers = parser.add_subparsers(help="sub-command help", dest="command")

    parser_prompt = subparsers.add_parser(
        "prompt",
        help="Get necessary information from a prompt",
    )
    parser_prompt.add_argument(
        "inventory_path",
        help="Path to machines inventory",
    )
    parser_prompt.add_argument(
        "--input_list",
        action="store_true",
        help="Allow to directly write inventory to in inventory_path instead of path",
    )

    parser_whitout_prompt = subparsers.add_parser(
        "whitout_prompt", help="Define directly necessary information as an argument"
    )
    parser_whitout_prompt.add_argument("ssh_username", help="Username for connection")
    parser_whitout_prompt.add_argument(
        "current_password", help="Current password for connection"
    )
    parser_whitout_prompt.add_argument("new_password", help="new password to set")
    parser_whitout_prompt.add_argument(
        "machines_list", help="List of machine must separate by comma"
    )

    return parser.parse_args()


# pylint: disable=R0914,R0915
# Main function containe a lot more variables from normal functions
async def main():
    """Main function"""
    config = configparser.ConfigParser()
    config.read("config.ini")

    log.configure_logger(config["log_level"]["level"] == "debug", LOG_OUTPUT_NAME)

    args = create_parser()

    if args.inventory_help:
        get_help_inventory_notion()

    if args.get_exemples:
        get_exemples()

    if args.inventory_help or args.get_exemples:
        sys.exit()

    if not args.command:
        log.print_orange("No subcommande used see help with change_pass -h")
        logging.critical("No subcommande used see help with change_pass -h")
        sys.exit()

    ssh_username = ""
    current_ssh_pass = ""
    new_pass = ""
    machines = []

    if args.command == "prompt":

        (
            ssh_username,
            current_ssh_pass,
            new_pass,
        ) = change_pass.interactive_mode_gatherer_information()

        machines = ""
        if args.input_list:

            machines = change_pass.interactive_mode_get_machines(
                change_pass.InputType.FROM_CLI, machines=args.inventory_path
            )
        else:
            machines = change_pass.interactive_mode_get_machines(
                change_pass.InputType.FROM_FILE,
                inventory_path=args.inventory_path,
            )

    elif args.command == "whitout_prompt":
        ssh_username = args.ssh_username
        current_ssh_pass = args.current_password
        new_pass = args.new_password
        machines = args.machines_list.split(change_pass.MACHINE_SEPARATOR)

    logging.debug(f"{ssh_username=}")
    logging.debug(f"{current_ssh_pass=}")
    logging.debug(f"{new_pass=}")
    logging.debug(f"{machines=}")

    patterns = (
        config["patterns"]["attempt_pattern_prompt1"],
        config["patterns"]["attempt_pattern_prompt2"],
        config["patterns"]["attempt_pattern_prompt3"],
        config["patterns"]["attempt_pattern_prompt4"],
    )

    functions = [
        change_pass.async_change_expired_password_over_ssh(
            host=machine,
            username=ssh_username,
            current_password=current_ssh_pass,
            new_password=new_pass,
            patterns=patterns,
        )
        for machine in machines
    ]

    results = await asyncio.gather(*functions)

    failed = []
    sucess = []
    for result in results:
        if result[1] == "sucess":
            sucess.append(result)
        else:
            failed.append(result)

    log.print_green(f"{len(sucess)}/{len(results)} password change")
    logging.info(f"{len(sucess)}/{len(results)} password change")

    if len(failed) == 0:
        return

    log.print_red(f"{len(failed)}/{len(results)} password change failed")
    logging.error(f"{len(failed)}/{len(results)} password change failed")

    widths = [max(map(len, col)) for col in zip(*failed)]
    header = ["Machine", "Status", "Error message"]

    output_data = ""
    output_data += "  ".join((val.ljust(width) for val, width in zip(header, widths)))
    for row in failed:
        output_data += "\n" + "  ".join(
            (val.ljust(width) for val, width in zip(row, widths))
        )

    output_file_path = os.path.join(
        USER_EXEC_PATH,
        f"{CURRENT_DATE.year}_{str(CURRENT_DATE.month).zfill(2)}_{str(CURRENT_DATE.day).zfill(2)}_error_log.txt",
    )
    with open(output_file_path, "w", encoding="utf-8") as file:
        file.write(output_data)
        file.write("\n\nInventory format for error machine:\n")
        file.write(",".join(data[0] for data in failed))

    log.print_green(f"Report error generated at {output_file_path}")
    log.print_green(f"Log generated at {LOG_OUTPUT_NAME}")


if __name__ == "__main__":

    asyncio.run(main())
